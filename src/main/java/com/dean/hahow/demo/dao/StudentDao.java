package com.dean.hahow.demo.dao;

import com.dean.hahow.demo.dto.Student;

public interface StudentDao {
    Student getStudentsById(Integer studentId);
}
