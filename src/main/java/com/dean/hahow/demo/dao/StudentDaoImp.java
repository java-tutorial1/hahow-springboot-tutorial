package com.dean.hahow.demo.dao;

import com.dean.hahow.demo.dto.Student;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class StudentDaoImp implements StudentDao {

    @Qualifier("testDataBaseJdbcTemplate")
    private NamedParameterJdbcTemplate testDataBaseJdbcTemplate;

    public StudentDaoImp(NamedParameterJdbcTemplate testDataBaseJdbcTemplate){
        this.testDataBaseJdbcTemplate = testDataBaseJdbcTemplate;
    }

    @Override
    public Student getStudentsById(Integer studebtId) {
        String sql = "SELECT ID,NAME FROM STUDENT WHERE ID = :studebtId";
        Map map = new HashMap<>();
        map.put("studebtId",studebtId);
        List<Student> list = testDataBaseJdbcTemplate.query(sql, map, (ResultSet rs) -> {

            List<Student> result = new ArrayList<>();
            while (rs.next()){
                Student student = new Student();
                student.setId(rs.getInt("id"));
                student.setName(rs.getString("Name"));
                result.add(student);
            }
            return result;

        });
        if (list.size() > 0)
            return list.get(0);
        else
            return null;
    }
}
