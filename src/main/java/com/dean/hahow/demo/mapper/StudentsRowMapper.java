package com.dean.hahow.demo.mapper;

import com.dean.hahow.demo.dto.Student;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudentsRowMapper implements RowMapper<Student> {
    @Override
    public Student mapRow(ResultSet resultSet, int i) throws SQLException {

        System.out.println("current row num: " +i);
        Student student = new Student();
        student.setId(resultSet.getInt("id"));
        student.setName(resultSet.getString("name"));
        System.out.println("student result: " + student);

        return student;
    }
}
