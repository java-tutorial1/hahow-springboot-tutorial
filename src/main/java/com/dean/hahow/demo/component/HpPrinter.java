package com.dean.hahow.demo.component;

import com.dean.hahow.demo.repository.IPrinter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class HpPrinter implements IPrinter {

    @Value("${my.name}")
    private String name;

    @Value("${count}")
    private int count ;

    //init member of bean hpPrinter
//    @PostConstruct
//    public void initCount(){
//        count = 5;
//        System.out.println("initialized count: "+count);
//    }


    @Override
    public void print(String message) {
        count--;
        System.out.println(name + ": " + message);
        System.out.println("剩餘次數: " + count);
    }


}
