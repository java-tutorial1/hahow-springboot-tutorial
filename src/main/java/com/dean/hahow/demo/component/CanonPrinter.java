package com.dean.hahow.demo.component;

import com.dean.hahow.demo.repository.IPrinter;

public class CanonPrinter implements IPrinter {
    @Override
    public void print(String message) {
        System.out.println("CanonPrinter: " + message);
    }
}
