package com.dean.hahow.demo.service;

import com.dean.hahow.demo.dao.StudentDao;
import com.dean.hahow.demo.dto.Student;
import org.springframework.stereotype.Component;

@Component
public class StudentServiceImp implements StudentService {
    private StudentDao studentDao;

    public StudentServiceImp(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    @Override
    public Student getStudentsById(Integer studebtId) {
        return studentDao.getStudentsById(studebtId);
    }
}
