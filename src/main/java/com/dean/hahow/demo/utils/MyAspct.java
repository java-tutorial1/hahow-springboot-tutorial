package com.dean.hahow.demo.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyAspct {

//    @Before("execution(* com.dean.hahow.demo.component.HpPrinter.*(..))")
//    public void before(){
//        System.out.println("I am @Before");
//    }
//
//    @After("execution(* com.dean.hahow.demo.component.HpPrinter.*(..))")
//    public void after(){
//        System.out.println("I am @After");
//    }

    @Around("execution(* com.dean.hahow.demo.component.HpPrinter.*(..))")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("I am @Before");

        //執行切入點方法
        Object obj = pjp.proceed();
        System.out.println("obj: " + obj);

        System.out.println("I am @After");
        return obj;
    }
}
