package com.dean.hahow.demo.repository;

import com.dean.hahow.demo.dto.StudentJpa;
import org.springframework.data.repository.CrudRepository;

public interface StudentRepository extends CrudRepository<StudentJpa, Integer> {
}
