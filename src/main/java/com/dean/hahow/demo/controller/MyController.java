package com.dean.hahow.demo.controller;

import com.dean.hahow.demo.dto.Store;
import com.dean.hahow.demo.dto.Student;
import com.dean.hahow.demo.dto.StudentJpa;
import com.dean.hahow.demo.mapper.StudentsRowMapper;
import com.dean.hahow.demo.repository.IPrinter;
import com.dean.hahow.demo.repository.StudentRepository;
import com.dean.hahow.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Validated
@RestController
public class MyController {

    @Qualifier("hpPrinter")
    private final IPrinter printer;

    private final StudentService studentService;

    @Qualifier("testDataBaseJdbcTemplate")
    private final NamedParameterJdbcTemplate testDataBaseJdbcTemplate;

    private final StudentRepository studentRepository;

    public MyController(IPrinter printer,
                        StudentService studentService,
                        NamedParameterJdbcTemplate testDataBaseJdbcTemplate,
                        StudentRepository studentRepository) {
        this.printer = printer;
        this.studentService = studentService;
        this.testDataBaseJdbcTemplate = testDataBaseJdbcTemplate;
        this.studentRepository = studentRepository;
    }

    @RequestMapping(value = "/test")
    public String test(){
        printer.print("Hello World! from printer");
        return "Hello World!";
    }

    @RequestMapping( "/product" )
    public Store product(@RequestParam(defaultValue = "defaultApple" ) String prodName ,@RequestParam(required = false,defaultValue = "noCostParam") String cost){
        System.out.println("product name: "+prodName);
        System.out.println("cost: " + cost);
        Store store = new Store();
        ArrayList<String> list = new ArrayList<>();
        list.add("蘋果");
        list.add("香蕉");
        store.setProductList(list);
        return store;
    }

    @RequestMapping("/test2")
    public String test2(@RequestBody Student student){
        System.out.println("id: " + student.getId());
        System.out.println("name: " + student.getName());
        return "Hello World!!";
    }

    @RequestMapping("/test3")
    public String test3(@RequestHeader(name = "Content-Type") String contentType){
        System.out.println("contentType: " + contentType);
        return "Hello world!!";
    }

    @RequestMapping("test4/{id}")
    public String test4(@PathVariable Integer id ){
        System.out.println("id: " + id);
        return "test4 finish";
    }

    //4-12 實作 RESTful API
    @PostMapping("/students")
    public String create(@RequestBody @Valid Student student){
        return "執行資料庫 Create 操作";
    }

    @GetMapping("/students/{id}/{name}")
    public String getStudents(@PathVariable @Min(100) Integer id, @PathVariable String name ){
        System.out.println("id: " + id);
        System.out.println("name: " + name);
        return "執行資料庫的 Read 操作";
    }

    @PutMapping("/students/{id}")
    public String updateStudent(@PathVariable Integer id, @RequestBody Student student){
        return "執行資料庫的 Update 操作";
    }

    @DeleteMapping("students/{id}")
    public String deleteStudent(@PathVariable Integer id){
        return "執行資料庫 Delete 操作";
    }

    //4-14 HTTP STATUS CODE customized by ResponseEntity
    @RequestMapping("/httpstatuscode")
    public ResponseEntity<String > responseEntity(){
        return ResponseEntity.status(HttpStatus.ACCEPTED).body("Hellow world!!!");
    }

    //4-15
    @RequestMapping("/exception")
    public String testExceptionHandler(){
        throw new IllegalArgumentException("testExceptionHandler Mether Error !!!");
    }

    //4-16
    @RequestMapping("/interceptor")
    public String passedInterceptor(){
        System.out.println("execute passedInterceptor method after passing Interceptor");
        return "PASSED";
    }

    //5-4
    @PostMapping("/insertStudents")
    public String insert(@RequestBody Student student){
        String sql = "INSERT INTO student (name) VALUE(:studentName)";

        Map<String,Object> map = new HashMap<>();
        map.put("studentName",student.getName());

        KeyHolder keyHolder = new GeneratedKeyHolder();

//        testDataBaseJdbcTemplate.update(sql, new MapSqlParameterSource(map),keyHolder);

        int key = keyHolder.getKey().intValue();
        System.out.println("Auto-generated key by mysql: "+key ) ;

        return "INSERT SQL SUCCESS!!";
    }

    @PostMapping("/students/batch")
    public String insertStudents(@RequestBody List<Student> studentList){
        System.out.println("enter insertStudents method");
        String sql = "INSERT INTO student(name) VALUE(:studentName)";

        MapSqlParameterSource[] parameterSource = new MapSqlParameterSource[studentList.size()];
        for(int i=0;i<studentList.size();i++){
            Student student = studentList.get(i);
            parameterSource[i] = new MapSqlParameterSource();
            parameterSource[i].addValue("studentName",student.getName());
        }
        testDataBaseJdbcTemplate.batchUpdate(sql,parameterSource);
        return "Insert students batch successed!!";
    }

    //5-6
    @RequestMapping("/students/selectAll")
    public List<Student> selStudents(){
        String sql="SELECT ID,NAME FROM STUDENT";
        Map map = new HashMap<>();
        List<Student> list = null;
        list = testDataBaseJdbcTemplate.query(sql, map, new StudentsRowMapper());
        System.out.println("list result: " + list);
        return list;
    }

    //5-7
    @RequestMapping("/students/{studebtId}")
    public Student selelctStudents(@PathVariable Integer studebtId){
        return studentService.getStudentsById(studebtId);
    }

    @PostMapping("/insStudent")
    public String insertStudentByName(@RequestBody StudentJpa studentJpa){
        studentRepository.save(studentJpa);
        return"insert student completed";
    }

}
