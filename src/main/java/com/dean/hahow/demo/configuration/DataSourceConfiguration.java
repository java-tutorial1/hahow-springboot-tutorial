package com.dean.hahow.demo.configuration;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

/**
 * THis class is a configuration that is set to multi JDBC connection
 */
//@Configuration
public class DataSourceConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.testdatabase")
    public DataSource testDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean
    public NamedParameterJdbcTemplate testDataBaseJdbcTemplate(@Qualifier("testDataSource") DataSource dataSource){
        return new NamedParameterJdbcTemplate(dataSource);
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.testdatabase2")
    public DataSource testDataSource2(){
        return DataSourceBuilder.create().build();
    }

    @Bean
    public NamedParameterJdbcTemplate testDataBase2JdbcTemplate(@Qualifier("testDataSource2") DataSource dataSource){
        return new NamedParameterJdbcTemplate(dataSource);
    }

}
