package com.dean.hahow.demo.configuration;

import com.dean.hahow.demo.component.HpPrinter;
import com.dean.hahow.demo.repository.IPrinter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

//@Configuration
public class MyConfiguration {

    @Bean
    public IPrinter myPrinter(){

        return new HpPrinter();
    }
}