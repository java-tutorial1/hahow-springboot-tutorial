package com.dean.hahow.demo.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class MyExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> handle(RuntimeException re){
        return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE)
                .body("RuntimeException: " + re.getMessage());
    }

//    @ExceptionHandler(IllegalArgumentException.class)
//    public ResponseEntity<String> handle(IllegalArgumentException iar){
//        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
//                .body("IlligalException: " + iar.getMessage());
//    }
}
